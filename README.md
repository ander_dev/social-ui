## Social UI

### Is a UI written in ReactJS designed to keep up with technology

Social UI is using:

* Trello to organise and manage project development (https://trello.com/b/lDSTblFv/westy-board)
* ReactJS
* Redux
* Material-UI
* docker and docker-compose
* nginx to start server in the docker

### Requirements:
* nodeJS to run it locally
* download the code from repo

## How to run:

### to run UI locally:
1. at the project folder run  

* ``npm install``
* ``npm start``
* if browser do not open with the application, please access it at http://localhost:3000

2. to compose docker container run  

* at project folder run: 
* to start: ``docker-compose up -d``

## to Access the UI
* after docker composed
* access http://localhost
* you will be required to create new user
* as well as login in the application, that will redirect you to the dashboard
* where you will be able to create posts and when selecting post created you will be able to create comments for it
* note that we have an issue with comments that are not getting refreshed in the posts lists, we need to click on the list to get it refreshed