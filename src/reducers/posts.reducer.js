import {postConstants} from "../resources/constants/post.constants"

export function posts(state = {}, action) {

  switch (action.type) {
    case postConstants.GETALL_REQUEST:
      return {
        loading: true
      }
    case postConstants.GETALL_SUCCESS:
      return {
        posts: action.posts,
      }
    case postConstants.GETALL_FAILURE:
      return {
        error: action.error
      }
    case postConstants.POST_REGISTER_REQUEST:
      return {
        savingPost: true
      }
    case postConstants.COMMENT_REGISTER_SUCCESS:
    case postConstants.POST_REGISTER_SUCCESS:
    case postConstants.POST_DELETE_SUCCESS:
      return {
        posts: action.posts
      }
    case postConstants.POST_REGISTER_FAILURE:
      return {
        error: action.error
      }
    case postConstants.COMMENT_REGISTER_REQUEST:
      return {
        savingContent: true
      }
    case postConstants.COMMENT_REGISTER_FAILURE:
      return {
        error: action.error
      }
    case postConstants.POST_DELETE_REQUEST:
      return {
        deletingPost: true
      }
    case postConstants.POST_DELETE_FAILURE:
      return {
        error: action.error
      }
    default:
      return state
  }
}