import {postConstants} from '../resources/constants/post.constants'
import {postService} from '../services/post.service'
import {history} from '../resources/history'
import Alert from 'react-s-alert'

export const postActions = {
    getAll,
    register,
    registerComment,
    deletePost
}

function getAll() {
    return dispatch => {
        dispatch(request())
        postService.getAll()
            .then(
                data => {
                    let message = data.messages[0]
                    if (data.statusCode !== 200) {
                        Alert.error(message, {
                            position: 'top-right'
                        })
                    } else {
                        let posts = data.objects
                        dispatch(success(posts))
                        history.push('/')
                    }
                },
                error => {
                    dispatch(failure(error))
                }
            )
    }

    function request() {
        return {type: postConstants.GETALL_REQUEST}
    }

    function success(posts) {
        return {type: postConstants.GETALL_SUCCESS, posts}
    }

    function failure(error) {
        return {type: postConstants.GETALL_FAILURE, error}
    }
}

function register(post, posts) {
    return dispatch => {
        dispatch(request(post))
        postService.register(post)
            .then(
                data => {
                    let messages = data.messages
                    if(data.statusCode !== 200){
                        Alert.error(messages, {
                            position: 'bottom-right'
                        })
                    }else {
                        let post = data.objects[0]
                        posts.unshift(post)
                        dispatch(success(posts))
                        history.push('/')
                        Alert.success(messages, {
                            position: 'bottom-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    Alert.error(error, {
                        position: 'bottom-right'
                    })
                }
            )
    }

    function request(post) {
        return {type: postConstants.POST_REGISTER_REQUEST, post}
    }

    function success(posts) {
        return {type: postConstants.POST_REGISTER_SUCCESS, posts}
    }

    function failure(error) {
        return {type: postConstants.POST_REGISTER_FAILURE, error}
    }
}

function registerComment(comment, posts) {
    return dispatch => {
        dispatch(request(comment))
        postService.registerComment(comment)
            .then(
                data => {
                    let messages = data.messages
                    if (data.statusCode !== 200) {
                        Alert.error(messages, {
                            position: 'bottom-right'
                        })
                    } else {
                        let comment = data.objects[0]
                        posts.forEach((post) => {
                            if (post.id === comment.post.id) {
                                post.commentList.unshift(comment)
                            }
                        })
                        dispatch(success(posts))
                        history.push('/')
                        Alert.success(messages, {
                            position: 'bottom-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    Alert.error(error, {
                        position: 'bottom-right'
                    })
                }
            )
    }

    function request(comment) {
        return {type: postConstants.COMMENT_REGISTER_REQUEST, comment}
    }

    function success(posts) {
        return {type: postConstants.COMMENT_REGISTER_SUCCESS, posts}
    }

    function failure(error) {
        return {type: postConstants.COMMENT_REGISTER_FAILURE, error}
    }
}

function deletePost(post, posts){
    return dispatch => {
        dispatch(request(post))
        postService.deletePost(post)
            .then(
                data => {
                    let messages = data.messages
                    if(data.statusCode !== 200){
                        Alert.error(messages, {
                            position: 'bottom-right'
                        })
                    }else {

                        const index = posts.indexOf(post);
                        if (index > -1) {
                            posts.splice(index, 1);
                        }
                        dispatch(success(posts))
                        history.push('/')
                        Alert.success(messages, {
                            position: 'bottom-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    Alert.error(error, {
                        position: 'bottom-right'
                    })
                }
            )
    }

    function request(post) {
        return {type: postConstants.POST_DELETE_REQUEST, post}
    }

    function success(posts) {
        return {type: postConstants.POST_DELETE_SUCCESS, posts}
    }

    function failure(error) {
        return {type: postConstants.POST_DELETE_FAILURE, error}
    }
}