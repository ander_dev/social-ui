import {userConstants} from '../resources/constants/user.constants'
import {userService} from '../services/user.service'
import {history} from '../resources/history'
import Alert from 'react-s-alert'

export const userActions = {
    login,
    logout,
    register
}

function login(user) {
    return dispatch => {
        dispatch(request(user))
        userService.login(user)
            .then(
                data => {
                    let message = data.messages[0]
                    if(data.statusCode !== 200){
                        Alert.error(message, {
                            position: 'bottom-right'
                        })
                    }else {
                        let user = data.objects[0]
                        dispatch(success(user))
                        localStorage.setItem('user', JSON.stringify(user))
                        history.push('/')
                        Alert.success(message, {
                            position: 'bottom-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                }
            )
    }

    function request(user) {
        return {type: userConstants.LOGIN_REQUEST, user}
    }

    function success(user) {
        return {type: userConstants.LOGIN_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.LOGIN_FAILURE, error}
    }
}

function logout() {
    userService.logout()
    return {type: userConstants.LOGOUT}
}

function register(user) {
    return dispatch => {
        dispatch(request(user))
        userService.register(user)
            .then(
                data => {
                    if(data.statusCode !== 200){
                        Alert.error(data.data.messages, {
                            position: 'top-right'
                        })
                    }else {
                        dispatch(success())
                        history.push('/')
                        Alert.success(data.messages, {
                            position: 'top-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    Alert.error(error, {
                        position: 'top-right'
                    })
                }
            )
    }

    function request(user) {
        return {type: userConstants.REGISTER_REQUEST, user}
    }

    function success(user) {
        return {type: userConstants.REGISTER_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.REGISTER_FAILURE, error}
    }
}