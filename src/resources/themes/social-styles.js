import withStyles from "@material-ui/core/styles/withStyles"
import {grey500, white} from '@material-ui/core/colors/index'

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        height: '100vh',
        border: '0px dotted black'
    },
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    itemHover: {
        '&:hover': {
            backgroundColor: theme.palette.grey[200],
        },
    },
    listContainer: {
        minWidth: 300,
        maxWidth: 500,
        position: 'absolute',
        top: '25%',
        left: 0,
        right: 0,
        margin: 'auto',
        border: '0px dotted black'
    },
    formContainer: {
        minWidth: 800,
        maxWidth: 1000,
        height: 'auto',
        position:'absolute',
        top:30,
        left: 0,
        right: 0,
        margin: 'auto',
        border: '0px dotted black',
    },
    homeContainer: {
        minWidth: 300,
        maxWidth: 500,
        height: 'auto',
        position: 'absolute',
        top: '35%',
        left: 0,
        right: 0,
        margin: 'auto',
        border: '0px dotted black',
    },
    button: {
        margin: 5,
    },
    leftIcon: {
        marginRight: 5,
    },
    rightIcon: {
        marginLeft: 5,
    },
    iconSmall: {
        fontSize: 20,
    },
    titleBox: {
        minWidth: 140,
        maxWidth: 220,
        height: 'auto',
        position: 'absolute',
        top: '25%',
        left: 0,
        right: 0,
        margin: 'auto',
        backgroundColor: 'white',
        border: '0px dotted black',
        borderRadius: '10px',
    },
    logoSize: {
        position: 'absolute',
        top: '10px',
        width: '40px',
        border: '0px dotted black',
        padding: '5px 5px 5px 5px',
    },
    titleStyle: {
        border: '0px dotted black',
        marginLeft: '40px',
        fontStyle: 'italic'
    },
    paper: {
        padding: 20,
        overflow: 'auto',
        minHeight: 'auto'
    },
    buttonsDiv: {
        textAlign: 'center',
        padding: 10,
    },
    flatButton: {
        color: grey500
    },
    postBtn: {
        float: 'right',
        width: '125px',
        marginTop: '25px',
        marginRight: '30px'
    },
    saveBtn: {
        float: 'right',
        marginTop: '25px',
        marginRight: '30px'
    },
    backBtn: {
        float: 'left',
        marginTop: '25px',
        marginLeft: '30px'
    },
    rememberMeBtn: {
        float: 'left',
        marginTop: '22px',
        marginLeft: '15px'
    },
    btn: {
        background: '#4f81e9',
        color: white,
        padding: 7,
        borderRadius: 2,
        margin: 2,
        fontSize: 13
    },
    btnSpan: {
        marginLeft: 5
    },
    registerLoginTitle: {
        marginLeft: 5,
        marginBottom: 25
    },
    menuItem: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& $primary, & $icon': {
                color: theme.palette.common.white,
            },
        },
    },
    container: {
        maxHeight: 440,
    },
    leftContainer:{
        maxHeight: 440,
        float: 'left',
        marginTop: '50px',
        marginLeft: '10px',
        border: '0px dotted black',
    },
    rightContainer:{
        maxHeight: 440,
        float: 'right',
        marginTop: '50px',
        marginLeft: '10px',
        border: '0px dotted black',
    }
})

export const SocialStyles = withStyles(styles)