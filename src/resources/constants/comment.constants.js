export const commentConstants = {

    COMMENT_REGISTER_REQUEST: 'COMMENT_REGISTER_REQUEST',
    COMMENT_REGISTER_SUCCESS: 'COMMENT_REGISTER_SUCCESS',
    COMMENT_REGISTER_FAILURE: 'COMMENT_REGISTER_FAILURE',

}