import axios from 'axios'

export const postService = {
    getAll,
    register,
    registerComment,
    deletePost
}

let URL = window.location.hostname

let axiosConfig = {
    baseURL: `http://${URL}:8086/api`,
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
    }, auth: {
        username: 'social_api_user',
        password: 'social_api_pass'
    }
}

function getAll() {
    return axios.get('/post', axiosConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.data
        })
}

function register(post) {
    return axios.post('/post', post, axiosConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}

function registerComment(post) {
    return axios.post('/comment', post, axiosConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}

function deletePost(post) {
    return axios.delete(`/post/${post.id}/${post.user.id}`, axiosConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}