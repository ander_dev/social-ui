import axios from 'axios'

export const userService = {
    login,
    logout,
    register
}

let URL = window.location.hostname

let axiosConfig = {
    baseURL: `http://${URL}:8086/api/user`,
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
    }, auth: {
        username: 'social_api_user',
        password: 'social_api_pass'
    }
}

let axiosLoginConfig = {
    baseURL: `http://${URL}:8086/api/user`,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    }, auth: {
        username: 'social_api_user',
        password: 'social_api_pass'
    }
}

function login(user) {
    let params = new URLSearchParams()
    params.append('email', user.email)
    params.append('password', user.password)
    return axios.post('/login', params, axiosLoginConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user')
}

function register(user) {
    return axios.post('', user, axiosConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}