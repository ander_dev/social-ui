import React, {Component} from 'react'
import {SocialStyles} from '../resources/themes/social-styles'
import Login from './public/login'
import {history} from '../resources/history'
import {Route, Router} from 'react-router-dom'
import {PrivateRoute} from './private.route'
import Register from './public/register'
import Alert from 'react-s-alert'
import 'react-s-alert/dist/s-alert-default.css'
import './app.css'
import Home from './restricted/home'

class App extends Component {

    render() {
        const { classes } = this.props

        return (
            <div className={classes.root}>
                <Alert stack={true} timeout={1500} />

                <Router history={history}>
                    <div>
                        <PrivateRoute exact path="/" component={Home} />
                        <Route path="/login" render={ (props) => <Login {...props} classes={classes}/>} />
                        <Route path="/register" render={ (props) => <Register {...props} classes={classes}/>} />
                    </div>
                </Router>
            </div>
        )
    }
}

export default SocialStyles(App)
