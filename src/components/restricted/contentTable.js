import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import connect from "react-redux/es/connect/connect"
import {postActions} from "../../actions/post.actions"
import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator'
import Button from "@material-ui/core/Button"
import Link from "@material-ui/core/Link"
import InsertCommentIcon from '@material-ui/icons/InsertComment'
import image from "../../resources/images/viking.jpg"
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'

class ContentTable extends Component {

    constructor(props) {
        super(props)
        this.state = {
            localPost: {
                message: ''
            },
            localComment: {
                message: '',
                post: {
                    id: ''
                }
            },
            selectedPost: {
                id: ''
            },
            refreshPost: false,
        }

        this.handleChangePost = this.handleChangePost.bind(this)
        this.handleDeletePost = this.handleDeletePost.bind(this)
        this.handleChangeComment = this.handleChangeComment.bind(this)
        this.handleSubmitPost = this.handleSubmitPost.bind(this)
        this.handleSubmitComment = this.handleSubmitComment.bind(this)
        this.handleSelectPost = this.handleSelectPost.bind(this)
    }

    componentDidMount() {
        const {dispatch} = this.props
        dispatch(postActions.getAll())
    }

    handleChangePost(event) {
        const {localPost} = this.state
        localPost.message = event.target.value
        this.setState({localPost});
    }

    handleChangeComment(event) {
        const {localComment} = this.state
        localComment.message = event.target.value
        this.setState({localComment});
    }

    handleSubmitPost(event) {
        event.preventDefault()

        const {localPost} = this.state
        const {dispatch, user, posts} = this.props
        localPost.user = user

        dispatch(postActions.register(localPost, posts))

        this.setState({
            localPost: {
                message: ''
            }
        })
    }

    handleSubmitComment(event) {
        event.preventDefault()

        const {localComment, selectedPost} = this.state
        const {dispatch, user, posts} = this.props
        localComment.user = user
        localComment.post.id = selectedPost.id

        dispatch(postActions.registerComment(localComment, posts))
        console.log('refresh before post: ', this.state.refreshPost)
        this.setState({
            localComment: {
                message: '',
                post: {
                    id: ''
                }
            },
            selectedPost: {
                id: ''
            },
            refreshPost: true,
        })
        console.log('refresh after post: ', this.state.refreshPost)
    }

    handleSelectPost(value) {
        const {selectedPost, comments} = this.state
        selectedPost.id = value.id
        this.setState({selectedPost, comments})
    }

    handleDeletePost(value) {
        const {dispatch, posts} = this.props
        dispatch(postActions.deletePost(value, posts))
    }

    render() {

        const {classes, posts, user} = this.props
        const {localPost, localComment, selectedPost} = this.state

        let requiredField = "Required Field!"

        return (
            <div>
                <List key="postList" className={classes.listContainer} id="posts">
                    <ValidatorForm ref="form" onSubmit={this.handleSubmitPost}>
                        <TextValidator
                            id="postMessage"
                            name="newPost"
                            label={"New Post"}
                            placeholder={"New Post"}
                            fullWidth={true}
                            onChange={this.handleChangePost}
                            value={localPost.message}
                            validators={['required']}
                            errorMessages={[requiredField]}
                        />
                        <Button className={classes.flatButton} type="submit"> </Button>
                    </ValidatorForm>
                    {posts !== undefined && posts.map((post) => {
                        return (
                            <div key={post.id}>
                                <ListItem alignItems="flex-start" key={post.id} className={classes.itemHover}>
                                    <ListItemAvatar>
                                        <Avatar alt="AN" src={image}/>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={
                                            <div>
                                                <React.Fragment>
                                                    <Typography
                                                        component={'span'}
                                                        variant={'subtitle1'}
                                                        className={classes.inline}
                                                        color="textPrimary">
                                                        {post.user.name} @ {post.created}
                                                        {user.id === post.user.id &&
                                                        <Link to="/" onClick={this.handleDeletePost.bind(this, post)}>
                                                            <Button className={classes.flatButton}>
                                                                <DeleteForeverIcon className={classes.rightIcon}/>
                                                            </Button>
                                                        </Link>
                                                        }
                                                        <br/>
                                                    </Typography>
                                                    {post.message}
                                                </React.Fragment>
                                            </div>
                                        }
                                        secondary={
                                            <span >
                                                <Link to="/" onClick={this.handleSelectPost.bind(this, post)}>
                                                    <Button className={classes.flatButton}>
                                                        <InsertCommentIcon className={classes.leftIcon} />
                                                        {"Add Comment"}
                                                    </Button>
                                                </Link>
                                                {!selectedPost.id !== '' && selectedPost.id === post.id &&
                                                <ValidatorForm ref="formComment" onSubmit={this.handleSubmitComment}>
                                                    <TextValidator
                                                        id="commentMessage"
                                                        name="newComment"
                                                        label={"New Comment"}
                                                        placeholder={"New Comment"}
                                                        fullWidth={true}
                                                        onChange={this.handleChangeComment}
                                                        value={localComment.message}
                                                        validators={['required']}
                                                        errorMessages={[requiredField]}
                                                    />
                                                    <Button className={classes.flatButton} type="submit">
                                                    </Button>
                                                </ValidatorForm>}
                                                {post.commentList !== undefined && post.commentList.map((comment) => {
                                                    return (
                                                        <div key={comment.id}>
                                                            <React.Fragment>
                                                                <Typography
                                                                    component={'span'}
                                                                    variant={'subtitle1'}
                                                                    className={classes.inline}
                                                                    color="textPrimary">
                                                                    {comment.user.name}
                                                                </Typography>
                                                                {" - " + comment.message}
                                                            </React.Fragment>
                                                        </div>
                                                    )
                                                })}
                                            </span>
                                        }
                                    />
                                </ListItem>
                                <Divider variant="inset" component="li"/>
                            </div>
                        )
                    })}
                </List>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const {posts, post, comment} = state.posts
    const {authentication} = state
    const {user} = authentication
    return {
        posts, user, post, comment
    }
}

export default connect(mapStateToProps)(ContentTable)