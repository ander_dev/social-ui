import React, {Component} from 'react'
import {Link} from "react-router-dom"
import {store} from "../../resources/store"
import {userActions} from "../../actions/user.actions"
import connect from "react-redux/es/connect/connect"

class Dashboard extends  Component {

    handleLogout() {
        store.dispatch(userActions.logout())
    }

    render() {

        const { user } = this.props

        return (

            <div className='content'>
                <div className="col-md-6 col-md-offset-3">
                    <h1>Hi {user.name}!</h1>
                    <p>You're logged in with React!!</p>

                    <p>
                        <Link to="/login" onClick={this.handleLogout}>Logout</Link>
                    </p>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { authentication } = state
    const { user } = authentication
    return {
        user
    }
}
export default connect(mapStateToProps)(Dashboard)
