import React, {Component} from 'react'
import {connect} from 'react-redux'
import {SocialStyles} from "../../resources/themes/social-styles"
import Header from "./header"
import ContentTable from "./contentTable"

class Home extends Component {

    render() {
        const { classes } = this.props
        return (
            <div className={classes.root}>

                <Header classes={classes} />

                <div>
                    <ContentTable classes={classes}/>
                </div>

            </div>
        )
    }
}

function mapStateToProps(state) {
    const { authentication } = state
    const { user } = authentication
    return {
        user
    }
}

export default connect(mapStateToProps)(SocialStyles(Home))