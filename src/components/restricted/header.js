import React, {Component} from 'react'
import PropTypes from 'prop-types'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import {store} from "../../resources/store"
import {userActions} from "../../actions/user.actions"
import {Link} from "react-router-dom"
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom'

class Header extends Component {
    state = {
        anchorEl: null,
    }

    handleClose = () => {
        this.setState({anchorEl: null})
    }

    handleLogout() {
        store.dispatch(userActions.logout())
    }

    render() {
        const {classes} = this.props
        const {anchorEl} = this.state
        const open = Boolean(anchorEl)

        return (
            <header className="header">
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="subtitle1" color="inherit" className={classes.flex}>
                            <div>
                                <div className={classes.titleStyle}>
                                    <span className="softblue">{"Social"}</span>
                                    <span className="hardblue">{"UI"}</span>
                                </div>
                            </div>
                        </Typography>

                        <div>
                            <IconButton onClick={this.handleLogout} component={Link} to="/">
                                <MeetingRoomIcon/>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={open}
                                onClose={this.handleClose}
                            >
                                <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                                <MenuItem onClick={this.handleClose}>My account</MenuItem>
                            </Menu>
                        </div>
                    </Toolbar>
                </AppBar>
            </header>
        )
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default Header