import React, {Component} from 'react'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import PersonAdd from "@material-ui/icons/PersonAdd"
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {Link} from "react-router-dom"
import PublicHeader from './public.header'
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator"
import {userActions} from "../../actions/user.actions"
import connect from "react-redux/es/connect/connect"

class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            user: {
                email: '',
                password: ''
            },
            showPassword: false,
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        const { user } = this.state
        user[event.target.name] = event.target.value
        this.setState({ user })
    }

    handleSubmit(event) {
        event.preventDefault()

        const { user } = this.state
        const { dispatch } = this.props

        dispatch(userActions.login(user))
    }

    render() {
        const { classes } = this.props
        const { user, showPassword } = this.state

        let requiredField = "Required Field!"
        let invalidEmail = "Invalid e-mail address!"

        return (
            <div>
                <PublicHeader classes={classes}/>

                <div className={classes.homeContainer}>
                    <Paper className={classes.paper}>
                        <ValidatorForm ref="form" onSubmit={this.handleSubmit} >
                            <TextValidator
                                id="user.email"
                                name="email"
                                label={"E-mail"}
                                placeholder={"E-mail"}
                                fullWidth={true}
                                onChange={this.handleChange}
                                value={user.email}
                                validators={['required', 'isEmail']}
                                errorMessages={[requiredField, invalidEmail]}
                            />
                            <TextValidator
                                id="user.password"
                                name="password"
                                label={"Password"}
                                placeholder={"Password"}
                                fullWidth={true}
                                type={showPassword ? 'text' : 'password'}
                                onChange={this.handleChange}
                                value={user.password}
                                validators={['required']}
                                errorMessages={[requiredField]}
                            />

                            <Button variant="contained" color="primary" className={classes.saveBtn} type="submit">
                                <ExitToAppIcon className={classes.leftIcon} />
                                {"Login"}
                            </Button>

                            <Button variant="contained" color="primary" className={classes.backBtn} component={Link} to="/register">
                                <PersonAdd className={classes.leftIcon}/>
                                {"Register"}
                            </Button>

                        </ValidatorForm>
                    </Paper>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    }
}

export default connect(mapStateToProps)(Login)