import React, {Component} from 'react'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography/Typography'
import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator'
import Save from '@material-ui/icons/Save'
import Cancel from '@material-ui/core/internal/svg-icons/Cancel'
import {Link} from "react-router-dom"
import {userActions} from "../../actions/user.actions"
import {connect} from "react-redux"
import PublicHeader from './public.header'

class Register extends  Component {

    constructor(props) {
        super(props)

        this.state = {
            user: {
                name: '',
                email: '',
                password: ''
            },
            submitted: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleChange(event) {
        const { user } = this.state
        user[event.target.name] = event.target.value
        this.setState({ user })
    }


    handleSubmit(event) {
        event.preventDefault()

        const { user } = this.state;
        const { dispatch } = this.props;

        dispatch(userActions.register(user))
    }

    render() {
        const { classes } = this.props
        const { user } = this.state

        let requiredField = "Required Field!"
        let invalidEmail = "Invalid e-mail address!"

        return (
            <div>
                <PublicHeader classes={classes}/>

                <div className={classes.homeContainer}>

                    <Paper className={classes.paper}>
                        <Typography variant="subtitle1" color="inherit" className={classes.registerLoginTitle}>
                            {"New User"}
                        </Typography>

                        <ValidatorForm ref="form" onSubmit={this.handleSubmit} >
                            <TextValidator
                                id="user.name"
                                name="name"
                                label={"Name"}
                                placeholder={"Name"}
                                fullWidth={true}
                                onChange={this.handleChange}
                                value={user.name}
                                validators={['required']}
                                errorMessages={[requiredField]}
                            />
                            <TextValidator
                                id="user.email"
                                name="email"
                                label={"E-mail"}
                                placeholder={"E-mail"}
                                fullWidth={true}
                                onChange={this.handleChange}
                                value={user.email}
                                validators={['required', 'isEmail']}
                                errorMessages={[requiredField, invalidEmail]}
                            />
                            <TextValidator
                                id="user.password"
                                name="password"
                                label={"Password"}
                                placeholder={"Password"}
                                fullWidth={true}
                                type="password"
                                onChange={this.handleChange}
                                value={user.password}
                                validators={['required']}
                                errorMessages={[requiredField]}
                            />

                            <Button variant="contained"  color="primary" className={classes.saveBtn} type="submit">
                                <Save className={classes.leftIcon} />
                                {"Save"}
                            </Button>

                            <Button variant="contained" color="primary" className={classes.backBtn} component={Link} to="/">
                                <Cancel className={classes.leftIcon} />
                                {"Back"}
                            </Button>
                        </ValidatorForm>
                    </Paper>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration
    return {
        registering
    }
}

export default connect(mapStateToProps)(Register)