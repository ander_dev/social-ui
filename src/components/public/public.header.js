import React, {Component} from 'react'

class PublicHeader extends Component {

    render() {
        const { classes } = this.props

        return (
            <div>
                <div className={classes.titleBox}>
                    <div className={classes.titleStyle}>
                        <span className="fontSize40 softblue">{"Social"}</span>
                        <span className="fontSize40 hardblue">{"UI"}</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default PublicHeader